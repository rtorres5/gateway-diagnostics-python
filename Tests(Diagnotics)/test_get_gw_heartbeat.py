from unittest import TestCase


class TestGetHeartbeat(TestCase):

    def test_get_gw_heartbeat(self):

        import diagnostic_utils

        EXPECTED_KEYS = ['latitude', 'longitude', 'timestamp', 'battery_percentage', 'battery_minutes_remaining',
                         'gateway_id']

        test_dict = diagnostic_utils.get_gw_heartbeat(1)
        self.assertEqual(len(test_dict), 6, "Heartbeat should only contain 6 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Heartbeat should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Heartbeat keys cannot contain empty string "
                                                                     "values!")





