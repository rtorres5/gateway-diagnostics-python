from unittest import TestCase


class TestPerforMemoryTest(TestCase):

    def test_perform_memory_test(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['memory_test_result', 'memory_test_exception', 'memory_test_start_time',
                         'memory_test_end_time', 'memory_test_time_to_complete']

        test_dict = diagnostic_utils.perform_memory_test()

        self.assertEqual(len(test_dict), 5, "Memory Test should only create 5 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):
            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Memory Test should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):
            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Memory Test keys cannot contain empty string "
                                                                     "values!")

