from unittest import TestCase


class TestPerformStorageTest(TestCase):

    def test_perform_storage_test(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['storage_test_result', 'storage_test_exception', 'storage_test_start_time',
                         'storage_test_end_time', 'storage_test_time_to_complete']

        test_dict = diagnostic_utils.perform_storage_test()

        self.assertEqual(len(test_dict), 5, "Storage Test should only create 5 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Storage Test should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Storage Test keys cannot contain empty string "
                                                                     "values!")

