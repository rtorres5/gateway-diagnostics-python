from unittest import TestCase


class TestGetTimestamp(TestCase):

    def test_get_gw_timestamp(self):

        import diagnostic_utils

        test_dict = {}
        test_dict = diagnostic_utils.get_gw_timestamp(test_dict)
        self.assertEqual(len(test_dict), 1, "get_gw_timestamp should only return one key-value pair!")

        timestamp = test_dict['timestamp']
        self.assertEqual(type(timestamp), str, "timestamp should be stored as a String!")
