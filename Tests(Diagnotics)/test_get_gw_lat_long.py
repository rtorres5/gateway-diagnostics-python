from unittest import TestCase


class TestGetLatLong(TestCase):

    def test_get_gw_lat_long(self):

        import diagnostic_utils

        STATUE_OF_LIBERTY_LATITUDE = 40.68
        STATUE_OF_LIBERTY_LONGITUDE = -74.04
        RIVERWALK_LATITUDE = 24.49
        RIVERWALK_LONGITUDE = -98.47
        STEDWARDS_LATITUDE = 30.22
        STEDWARDS_LONGITUDE = -97.75

        test_dict = {}
        lat_long_dict = diagnostic_utils.get_gw_lat_long(test_dict)
        test_latitude = lat_long_dict["latitude"]
        test_longitude = lat_long_dict["longitude"]
        self.assertNotEqual(test_latitude, STATUE_OF_LIBERTY_LATITUDE, "Austin, TX and the Statue of Liberty have "
                                                                       "different latitudes!")
        self.assertNotEqual(test_latitude, RIVERWALK_LATITUDE, "Austin, TX and the Riverwalk have different latitudes!")
        self.assertNotEqual(test_latitude, STEDWARDS_LATITUDE, "Austin, TX and St. Edwards have different latitudes!")

        self.assertNotEqual(test_longitude, STATUE_OF_LIBERTY_LONGITUDE, "Austin, TX and the Statue of Liberty have "
                                                                         "different longitudes!")
        self.assertNotEqual(test_longitude, RIVERWALK_LONGITUDE, "Austin, TX and the Riverwalk have different "
                                                                 "longitudes!")
        self.assertNotEqual(test_longitude, STEDWARDS_LONGITUDE, "Austin, TX and St. Edwards have different "
                                                                 "longitudes!")
