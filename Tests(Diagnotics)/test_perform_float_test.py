from unittest import TestCase


class TestPerform_float_test(TestCase):

    def test_perform_float_test(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['float_test_result', 'float_test_exception', 'float_test_start_time',
                         'float_test_end_time', 'float_test_time_to_complete']

        test_dict = diagnostic_utils.perform_float_test()

        self.assertEqual(len(test_dict), 5, "Float Test should only create 5 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):
            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Float Test should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):
            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Float Test keys cannot contain empty string "
                                                                     "values!")
