from unittest import TestCase


class TestPerformPrimeTest(TestCase):

    def test_perform_prime_test(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['prime_test_result', 'prime_test_exception', 'prime_test_start_time',
                         'prime_test_end_time', 'prime_test_time_to_complete']

        test_dict = diagnostic_utils.perform_prime_test()

        self.assertEqual(len(test_dict), 5, "Prime Test should only create 5 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Prime Test should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Prime Test keys cannot contain empty string "
                                                                     "values!")

