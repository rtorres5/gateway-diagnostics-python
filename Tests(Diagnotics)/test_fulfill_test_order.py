from unittest import TestCase


class TestFulfill_test_order(TestCase):

    def test_fulfill_test_order(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['memory', 'storage', 'prime', 'integer', 'float', 'gateway_id']

        test_dict = diagnostic_utils.perform_daily_diagnostic(1)

        self.assertEqual(len(test_dict), 6, "Daily Diagnostic should only create 6 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):
            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Daily Diagnostic should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")
