from unittest import TestCase


class TestPerformIntegerTest(TestCase):

    def test_perform_integer_test(self):

        import diagnostic_utils

        test_dict = {}

        EXPECTED_KEYS = ['integer_test_result', 'integer_test_exception', 'integer_test_start_time',
                         'integer_test_end_time', 'integer_test_time_to_complete']

        test_dict = diagnostic_utils.perform_integer_test()

        self.assertEqual(len(test_dict), 5, "Integer Test should only create 5 key-pair values!")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertTrue(EXPECTED_KEYS[index] in test_dict, "Integer Test should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS)):

            self.assertNotEqual(test_dict[EXPECTED_KEYS[index]], "", "Integer Test keys cannot contain empty string "
                                                                     "values!")
