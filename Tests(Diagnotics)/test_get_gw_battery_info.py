from unittest import TestCase


class TestGetBatteryInfo(TestCase):

    def test_get_gw_battery_info(self):

        import diagnostic_utils

        test_dict = {}
        test_dict = diagnostic_utils.get_gw_battery_info(test_dict)
        battery_percentage = test_dict["battery_percentage"]
        battery_minutes_remaining = test_dict["battery_minutes_remaining"]
        self.assertGreater(float(battery_percentage), 0, "battery_percentage cannot be lower than 0!")
        self.assertGreater(float(battery_minutes_remaining), 0, "battery_minutes_remaining cannot be lower than 0!")
        self.assertLess(float(battery_percentage), 100, "battery_percentage cannot be higher than 100!")


