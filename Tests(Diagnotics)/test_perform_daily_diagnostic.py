from unittest import TestCase


class TestPerform_daily_diagnostic(TestCase):

    def test_perform_daily_diagnostic(self):

        import diagnostic_utils

        test_dict = {}

        TOTAL_KEYS = ['memory', 'storage', 'prime', 'integer', 'float', 'gateway_id']
        EXPECTED_KEYS_NESTED = ['memory', 'storage', 'prime', 'integer', 'float']

        test_dict = diagnostic_utils.perform_daily_diagnostic(1)

        self.assertEqual(len(test_dict), 6, "Daily Diagnostic should only create 6 key-pair values!")

        for index in range(0, len(TOTAL_KEYS)):

            self.assertTrue(TOTAL_KEYS[index] in test_dict, "Daily Diagnostic should contain a key-value pair for "
                                                               "all key names in EXPECTED_KEYS")

        for index in range(0, len(EXPECTED_KEYS_NESTED)):

            self.assertEqual(len(test_dict[EXPECTED_KEYS_NESTED[index]]), 5, "Daily Diagnostic should hold nested json with"
                                                                      "five result components per test!")
