# Gateway Diagnostics

## Setup:
This program is meant to function with its java component, 'Gateway-Controller-Maven'. 'Gateway-Diagnostics-Python' is meant to be run first.

## Description:

This component will open a socket server to the Gateway Controller. When a connection is established by the Gateway Controller, it will respond to test requests by performing the test that was requested, formatting it into JSON and sending it back to the Gateway Controller.


