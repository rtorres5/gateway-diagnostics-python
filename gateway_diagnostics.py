# File Name: gateway_diagnostics.py
# Purpose: Represents the Gateway Diagnostics component.
#          To create a local socket server for the Gateway Controller. Depending on the message received from the
#          Gateway Controller, this program will get the requested data, compile it into a dictionary, and send it
#          back to the Gateway Controller as a JSON format.
# Complete:
#       1. Creates the socket server on the localhost
#       2. Accept "Heartbeat" request from Gateway Controller - Return Heartbeat Info as JSON
#       3. Accept "Memory Test" request from Gateway Controller - Return Memory Test Info as JSON
#       4. Accept "Storage Test" request from Gateway Controller - Return Storage Result Info as JSON
#       5. Accept "Daily Diagnostic" request from Gateway Controller - Return Daily Diagnostic Info as JSON
#       6. Accept "Integer Test" request from Gateway Controller - Return Integer Result Info as JSON
#       7. Accept "Float Test" request from Gateway Controller - Return Float Result Info as JSON
#       8. Accept "Prime Test" request from Gateway Controller - Return Prime Result Info as JSON


import diagnostic_utils
import socket
import json

try:

    soc = socket.socket()
    host = "localhost"
    #host = "192.168.1.103"
    port = 2004
    soc.bind((host, port))
    soc.listen(5)


    print("***** This is the Gateway Diagnostics Socket Server *****")
    print("\nThis will receive input from Gateway Controller via a Socket Client")
    print("\n\nCurrently awaiting connection from Gateway Controller Client......")

    while True:

        conn, address = soc.accept()

        print("\nConnection established from: ", address)

        counter = 1

        while True:

            json_request_dict = {}
            json_result_dict ={}
            json_response_dict = {}
            gateway_id = ""
            request_name = ""

            length_of_message = int.from_bytes(conn.recv(2), byteorder="big")
            print("\n\nMessage Length From GW Control: " + str(length_of_message))
            message = conn.recv(length_of_message).decode("UTF-8")
            print("\nMessage From GW Control: " + message)
            print("\nConverting message to a dictionary")
            json_request_dict = json.loads(message)
            gateway_id = int(json_request_dict["gateway_id"])
            print("\nGateway Id: " + str(gateway_id))
            print("\nMessage as a dictionary: " + str(json_request_dict))
            request = json_request_dict["request"]
            print("\nJSON Request Dict['request']: " + request)

            if request == "gatewayHB":

                print(request + " request is being fulfilled....")
                json_result_dict = diagnostic_utils.get_gw_heartbeat(gateway_id)

            elif request == "daily_diagnostic":

                print(request + " request is being fulfilled....")
                json_result_dict["daily_diagnostic"] = diagnostic_utils.perform_daily_diagnostic(gateway_id)

            elif request == "custom_request":

                request_list = json_request_dict["custom_request"]
                request_list = diagnostic_utils.string_to_list(request_list)
                json_result_dict = diagnostic_utils.fulfill_custom_test_list(request_list, gateway_id)
                print(json_result_dict)

            json_response_dict = bytes(json.dumps(json_result_dict), "UTF-8")
            conn.send(len(json_response_dict).to_bytes(2, byteorder="big"))
            conn.send(json_response_dict)
            print("Diagnostic Response for Request #" + str(counter) + ": Complete!")

            counter += 1


except Exception as e:

    print(e)

finally:

    soc.close()
